import './App.css'
import ListBox from './components/ListBox/ListBox'

function App() {
  return (
    <div className="app">
      <ListBox></ListBox>
    </div>
  )
}

export default App
